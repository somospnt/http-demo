package com.somospnt.integration;

import com.somospnt.integration.domain.Feriado;
import java.util.HashMap;
import java.util.Map;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.http.HttpMethod;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.http.outbound.HttpRequestExecutingMessageHandler;
import org.springframework.integration.json.JsonToObjectTransformer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

@Configuration
@IntegrationComponentScan
public class IntegrationConfig {

    @Bean
    public Boolean procesamiento() {
        return Boolean.TRUE;
    }
    @Bean
    public MessageChannel httpChannel() {
        return new DirectChannel();
    }

    @Bean
    @ServiceActivator(inputChannel = "httpChannel")
    public MessageHandler httpHandler() {
        SpelExpressionParser expressionParser = new SpelExpressionParser();
        Map<String, Expression> uriVariableExpressions = new HashMap<>();
        uriVariableExpressions.put("anio", expressionParser.parseExpression("payload"));

        HttpRequestExecutingMessageHandler handler = new HttpRequestExecutingMessageHandler("http://apiday.somospnt.com/api/feriados/{anio}");
        handler.setExpectedResponseType(String.class);
        handler.setHttpMethod(HttpMethod.GET);
        handler.setOutputChannelName("transformChannel");
        handler.setUriVariableExpressions(uriVariableExpressions);

        return handler;
    }

    @Bean
    @Transformer(inputChannel = "transformChannel" , outputChannel = "feriadosChannel")
    public JsonToObjectTransformer jsonToObjectTransformer() {
        return new JsonToObjectTransformer(Feriado[].class);
    }

}
