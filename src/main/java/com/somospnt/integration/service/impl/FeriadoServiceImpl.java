package com.somospnt.integration.service.impl;

import com.somospnt.integration.domain.Feriado;
import com.somospnt.integration.repository.FeriadoRepository;
import com.somospnt.integration.service.FeriadoService;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FeriadoServiceImpl implements FeriadoService {

    @Autowired
    private FeriadoRepository feriadoRepository;

    @Resource(name = "httpChannel")
    private MessageChannel channel;

    @Resource(name = "procesamiento")
    private Boolean procesamiento;

    @Scheduled(fixedDelay = 10000)
    @Override
    public void procesar() {
        int anio = 2014;

        while (procesamiento) {
            System.out.println("Procesando año: " + anio);
            Message m = MessageBuilder.withPayload(anio).build();
            channel.send(m);
            anio = anio + 1;
        }
        
        procesamiento = true;
    }

    @ServiceActivator(inputChannel = "feriadosChannel")
    @Override
    public void guardar(List<Feriado> feriados) {
        if (feriados.size() > 0) {
            feriadoRepository.save(feriados);
        } else {
            procesamiento = false;
        }
    }

}
