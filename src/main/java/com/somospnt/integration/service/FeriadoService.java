package com.somospnt.integration.service;

import com.somospnt.integration.domain.Feriado;
import java.util.List;

public interface FeriadoService {
    
    void guardar(List<Feriado> feriados);
    void procesar();
    
}
