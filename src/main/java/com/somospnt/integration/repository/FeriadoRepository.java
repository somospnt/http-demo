package com.somospnt.integration.repository;

import com.somospnt.integration.domain.Feriado;
import org.springframework.data.repository.CrudRepository;

public interface FeriadoRepository extends CrudRepository<Feriado, Long>{
    
}
