DROP TABLE IF EXISTS feriado;

CREATE TABLE feriado (
    id BIGINT identity primary key,
    descripcion VARCHAR(255) NOT NULL,
    tipo VARCHAR(20) NULL,
    fecha VARCHAR(10) NOT NULL
);
